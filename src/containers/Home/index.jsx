import React from 'react';
import Notification from '../../components/Notification';
import HeroBanner from '../../components/HeroBanner';
import HighlightsPanel from '../../components/HighlightsPanel';
import Footer from '../../components/Footer';
import Newsletter from '../../components/Newsletter';

const Home = () => {
  return (
    <React.Fragment>
      <Notification />
      <HeroBanner />
      <HighlightsPanel />
      <Footer />
      <Newsletter />
    </React.Fragment>
  );
};

export default Home;
