import styled from 'styled-components'
import { theme } from '../../constants';

const Container = styled.div`
  background: ${theme.darkBlue};
  padding: 21px 0px;
`

const FooterText = styled.h4`
  font-size: 14px;
  text-align: center;
  margin: 0px;
  color: ${theme.defaultBlue};
`

export {
  Container,
  FooterText
}
