import React from 'react';
import { Container, FooterText } from './style.js';

const Footer = () => {
  return (
    <Container>
      <FooterText>&copy; 2018 Yuan Manos. All rights reserved.</FooterText>
    </Container>
  );
};

export default Footer;
