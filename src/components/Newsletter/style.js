import styled, { css } from 'styled-components';
import { theme, screen } from '../../constants';

const Container = styled.div`
  padding: 14px;
  background-color: ${theme.defaultBlue};
  opacity: 0.9;
  position: fixed;
  bottom: 0px;
  padding: 21px;
  width: calc(50% - 42px);
  @media ${screen.maxSmall} {
    width: calc(100% - 42px);
  }
`;

const Wrapper = styled.div`
  position: relative;
  width: 100%;
`;

const CtaClose = styled.div`
  color: #fafafafa;
  font-weight: bold;
  position: absolute;
  right: 0px;
  top: -14px;
  cursor: pointer;
`;

const Title = styled.h4`
  margin-top: 14px;
  color: #fafafafa;
  margin: 0px 0px 7px 0px;
  font-size: 20px;
`;

const Text = styled.h5`
  font-size: 14px;
  color: #fafafafa;
  margin: 0px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: row;
  margin-top: 14px;
  @media ${screen.maxSmall} {
    flex-direction: column;
  }
`;

const InputField = styled.input`
  background: #fafafa;
  border: 1px solid rgba(0, 0, 0, 0.125);
  font-size: 14px;
  flex: 1;
  padding: 5px;
  :focus {
    outline: none;
  }
`;

const ButtonForm = styled.button`
  color: #fafafafa;
  background: #ffab40;
  border: none;
  margin: 0px 7px;
  font-size: 14px;
  padding: 5px;
  :focus {
    outline: none;
  }
  @media ${screen.maxSmall} {
    margin: 7px 0px;
  }
`;

export {
  Container,
  Wrapper,
  Title,
  Text,
  CtaClose,
  Form,
  InputField,
  ButtonForm
};
