import React, { useState, useEffect } from 'react';
import Cookies from 'universal-cookie';
import { Transition } from 'react-transition-group';
import { cookie } from '../../constants';
import {
  Container,
  Wrapper,
  Title,
  Text,
  CtaClose,
  Form,
  InputField,
  ButtonForm
} from './style';

const NewsLetter = () => {
  const cookies = new Cookies();
  const [isDisplay, setIsDisplay] = useState(false);

  const duration = 300;

  const defaultStyle = {
    transition: `bottom ${duration}ms ease-in-out, opacity ${duration *
      2}ms ease-in-out`,
    opacity: 0
  };

  const transitionStyles = {
    entering: { opacity: 1, bottom: '0px' },
    entered: { opacity: 1, bottom: '0px' },
    exiting: { opacity: 0, bottom: '-100px', pointerEvents: 'none' },
    exited: { opacity: 0, bottom: '-100px', pointerEvents: 'none' }
  };

  useEffect(() => {
    const IS_CLOSED_NEWSLETTER = cookies.get(cookie.CLOSED_NEWSLETTER);
    if (!IS_CLOSED_NEWSLETTER) {
      setIsDisplay(true);
    }
  }, []);

  const expirationMinutes = minute => {
    var date = new Date();
    date.setTime(date.getTime() + 60 * 1000 * minute);
  };
  const closeNewsletter = () => {
    cookies.set(cookie.CLOSED_NEWSLETTER, true, {
      path: '/',
      expires: expirationMinutes(10)
    });
    setIsDisplay(false);
  };
  return (
    <React.Fragment>
      <Transition in={isDisplay} timeout={duration}>
        {state => (
          <div
            style={{
              ...defaultStyle,
              ...transitionStyles[state]
            }}
          >
            <Container>
              <Wrapper>
                <CtaClose onClick={closeNewsletter}>x</CtaClose>
                <Title>Get latest updates in web technologies</Title>
                <Text>
                  I write articles related to web technologies, such as design
                  trends, development tools, UI/UX case studies and reviews, and
                  more. Sign up to my newsletter to get them all.
                </Text>
                <Form submit="#">
                  <InputField placeholder="Email address" />
                  <ButtonForm type="submit">Count me in!</ButtonForm>
                </Form>
              </Wrapper>
            </Container>
          </div>
        )}
      </Transition>
    </React.Fragment>
  );
};

export default NewsLetter;
