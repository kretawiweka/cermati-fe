import styled from 'styled-components';
import HeroBannerImage from '../../assets/images/hero-banner.jpg';
import { theme } from '../../constants';

const Hero = styled.div`
  background-image: url(${HeroBannerImage});
  height: 100%;
  background-size: cover;
  min-height: 500px;
  position: relative;
`;

const HeroLayer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: ${theme.darkBlue};
  opacity: 0.7;
`;

const HeroLogo = styled.img`
  width: 25px;
  margin: 7px;
`;

const ContentContainer = styled.div`
  display: flex;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  flex-direction: column;
  align-items: center;
`;

const HeroButton = styled.button`
  color: ${theme.white};
  border: 1px solid ${theme.white};
  border-radius: 4px;
  background: transparent;
  padding: 7px 10px;
  cursor: pointer;
  margin-top: 14px;
  :hover {
    background: ${theme.white};
    color: ${theme.darkBlue};
  }
  :focus {
    outline: none;
  }
`;

const HeroGreeting = styled.h3`
  color: ${theme.white};
  margin: 3.5px;
  font-size: 24px;
  text-align: center;
  font-weight: 400;
`;

const HeroTitle = styled.h4`
  color: ${theme.white};
  margin: 3.5px 0px 7px 0px;
  font-size: 16px;
  font-weight: 600;
  text-align: center;
`;

const HeroSubtitle = styled.h5`
  color: ${theme.white};
  margin: 0px;
  font-size: 12px;
  text-align: center;
`;

export {
  Hero,
  HeroLogo,
  ContentContainer,
  HeroButton,
  HeroGreeting,
  HeroTitle,
  HeroSubtitle,
  HeroLayer
};
