import React from 'react';
import {
  Hero,
  HeroLogo,
  ContentContainer,
  HeroButton,
  HeroGreeting,
  HeroTitle,
  HeroSubtitle,
  HeroLayer
} from './style';

const HeroBanner = () => {
  return (
    <Hero>
      <HeroLayer>
        <HeroLogo src={require('../../assets/images/y-logo-white.png')} />
        <ContentContainer>
          <HeroGreeting>Hello! I'm Kretawiweka Nuraga Sani</HeroGreeting>
          <HeroTitle>Consult, Design, and Develop Websites</HeroTitle>
          <HeroSubtitle>
            Have something in mind? Feel free to cantact me.
          </HeroSubtitle>
          <HeroSubtitle>I'll help you to make it happen</HeroSubtitle>
          <HeroButton>LET'S MAKE CONTACT</HeroButton>
        </ContentContainer>
      </HeroLayer>
    </Hero>
  );
};

export default HeroBanner;
