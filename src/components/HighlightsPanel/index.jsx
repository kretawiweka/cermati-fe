import React from 'react';
import Card from '../Card';
import {
  HeaderHighlights,
  Container,
  Title,
  Subtitle,
  ContentWrapper,
  CardItem,
  Grid
} from './style';
import data from './data';

const HighlightsPanel = () => {
  return (
    <Container>
      <HeaderHighlights>
        <Title>How Can I Help You</Title>
        <Subtitle>
          Our work then targeted, best pracites outcomes social innovation
          synergy. Venture philanthropy, revolutionary inclusive policymaker
          relief. User centered program areas scale.
        </Subtitle>
      </HeaderHighlights>
      <ContentWrapper>
        {data.map((item, index) => (
          <Grid key={index}>
            <CardItem>
              <Card dataItem={item} />
            </CardItem>
          </Grid>
        ))}
      </ContentWrapper>
    </Container>
  );
};

export default HighlightsPanel;
