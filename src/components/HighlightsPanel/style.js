import styled from 'styled-components';
import { theme, screen } from '../../constants';

const Container = styled.div`
  background-color: ${theme.smokeGrey};
  padding: 21px 14px;
`;

const HeaderHighlights = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 7px;
`;

const Title = styled.h4`
  margin: 0px;
  font-size: 24px;
  text-align: center;
  color: ${theme.textColors.base};
`;

const Subtitle = styled.h5`
  text-align: center;
  max-width: 480px;
  color: ${theme.textColors.base};
`;

const Grid = styled.div`
  display: inline-block;
  @media ${screen.minLarge} {
    width: 33.33%;
  }
  @media ${screen.maxLarge} {
    width: 50%;
  }
  @media ${screen.maxSmall} {
    width: 100%;
  }
`

const CardItem = styled.div`
  margin: 14px 7px;
`;

const ContentWrapper = styled.div`

`;

export {
  HeaderHighlights,
  Container,
  Title,
  Subtitle,
  ContentWrapper,
  CardItem,
  Grid
};
