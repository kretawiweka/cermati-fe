import styled from 'styled-components';
import { theme, screen } from '../../constants';

const NotificationContainer = styled.div`
  background-color: ${theme.smokeGrey};
  padding: 7px;
`;

const TextNotification = styled.p`
  font-size: 12px;
  margin: 0px;
  @media ${screen.maxSmall} {
    margin: 7px 0px;
  }
  color: ${theme.textColors.base};
  a {
    text-decoration: none;
  }
`;

const ContentWrapper = styled.div`
  max-width: 480px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  align-items: center;
  @media ${screen.maxSmall} {
    align-items: end;
    flex-direction: column;
  }
`;

const CtaNotification = styled.button`
  border: none;
  border-radius: 4px;
  background-color: ${theme.defaultBlue};
  color: ${theme.white};
  font-size: 12px;
  height: 100%;
  padding: 7px;
  min-width: 75px;
  @media ${screen.minSmall} {
    margin: 3.5px;
  }
`;

export {
  NotificationContainer,
  TextNotification,
  ContentWrapper,
  CtaNotification
};
