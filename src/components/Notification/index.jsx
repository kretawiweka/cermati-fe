import React from 'react';
import {
  NotificationContainer,
  TextNotification,
  ContentWrapper,
  CtaNotification
} from './style';

const Notification = () => {
  return (
    <NotificationContainer>
      <ContentWrapper>
        <TextNotification>
          By accessing and using this website, you acknowledge that you have
          read and understand our <a href="#url-cookie-policy">Cookie Policy</a>
          , <a href="#url-privaci-policy">Privacy Policy</a>, and our{' '}
          <a href="#url-tos">Terms of Service</a>.
        </TextNotification>
        <CtaNotification>Got it</CtaNotification>
      </ContentWrapper>
    </NotificationContainer>
  );
};

export default Notification;
