import React from 'react';
import { CardWrapper, CardHeader, CardTitle, CardText } from './style';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Card = props => {
  return (
    <CardWrapper>
      <CardHeader>
        <CardTitle>{props.dataItem.title}</CardTitle>
        <FontAwesomeIcon icon={props.dataItem.icon} />
      </CardHeader>
      <CardText>{props.dataItem.text}</CardText>
    </CardWrapper>
  );
};

export default Card;
