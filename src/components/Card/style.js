import styled from 'styled-components';
import { theme } from '../../constants';

const CardWrapper = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.125);
  background-color: ${theme.smokeGrey};
  padding: 14px;
`;

const CardHeader = styled.div`
  margin-bottom: 7px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  color: #ababab;
`;

const CardTitle = styled.h4`
  margin: 0px;
  font-size: 22px;
  color: ${theme.textColors.base};
`;

const CardText = styled.p`
  margin: 0px;
  font-size: 14px;
  color: ${theme.textColors.base};
`;

export { CardWrapper, CardHeader, CardTitle, CardText };
