const size = {
  small: '480px',
  medium: '720px',
  large: '960px'
}

export const device = {
  minSmall: `(min-width: ${size.small})`,
  maxSmall: `(max-width: ${size.small})`,
  minMedium: `(min-width: ${size.medium})`,
  maxMedium: `(max-width: ${size.medium})`,
  minLarge: `(min-width: ${size.large})`,
  maxLarge: `(max-width: ${size.large})`,
};

export default device
