const theme = {
  defaultBlue: '#007bc1',
  darkBlue: '#004a75',
  defaultOrange: '#ff8000',
  darkOrange: '#cc6600',
  smokeGrey: '#e5e5e5',
  white: '#ffffff',
  textColors: {
    base: '#4a4a4a'
  }
};

export default theme;
