# Cermati-FE

Project build with [CRAFT](https://github.com/Kretawiweka/cra-template-craft) powered by [Create React App](https://github.com/facebook/create-react-app).

## Development

### Project Setup

```
git clone <repo-url>

yarn install
```

### Local Development

```
yarn start
```

### Test

```
yarn test
```

### Build for Production

```
yarn build
```

## Project Structure

```
src/
  ├── components/
  ├── constants/
  ├── containers/
  ├── pages/
  ├── styles/
  └── utils/
```

`/src/components` React Components

`/src/constants` Constants which used in this project

`/src/containers` For fetch and redux logic

`/src/pages` Pages of each routes

`/src/styles` Basic styles (normalize, etc) for this App

`/src/utils` Utils Helpers
